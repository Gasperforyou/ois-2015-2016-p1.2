// Spremenljivke
var ime = "Dejan";
var priimek = "Lavbič";
console.log("A ni lep dan " + ime + " " + priimek + "?")

// Logične operacije
console.log(13.2 === 13.2)
console.log("10" === 10)
console.log(10 < 5)

// Vejitve
var ocena = 49.8;
if (ocena >= 49.5) {
  console.log("Izpit ste uspešno opravili!");
} else {
  console.log("Na žalost izpit ni opravljen!");
}

// Zanke
var i=1;
while (i < 10) {
  console.log("while" + i);
  i = i+1;
}

for (var i=1; i < 10; i++) {
  console.log("for" + i);
}

// Funkcije
var siOpravilIzpit = function(ocena) {
  return (ocena >= 49.5) ? true : false;
}
console.log(siOpravilIzpit(49));
console.log(siOpravilIzpit(50));

// Objekti
var student = {
  ime: "Dejan",
  priimek: "Lavbič",
  ocena: 49,
  predstaviSe: function() {
    console.log("Sem " + this.ime + " " + this.priimek + " in sem pri izpitu iz OIS dosegel " + this.ocena + " točk.");
  }
}
student.predstaviSe();

// Vrste
var seznamZelja = ["anakonda", "kobra"];
seznamZelja.push("piton");    // Dodaj element
console.log(seznamZelja);
console.log(seznamZelja[1]);
seznamZelja.pop();            // Briši element
console.log(seznamZelja);
